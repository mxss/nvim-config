call plug#begin()

" Misc

Plug 'pangloss/vim-javascript'
Plug 'leafgarland/typescript-vim'
Plug 'peitalin/vim-jsx-typescript'
Plug 'styled-components/vim-styled-components', { 'branch': 'main' }
Plug 'maxmellon/vim-jsx-pretty'
Plug 'neoclide/coc.nvim', {'branch': 'release'}
" Plug 'neoclide/coc.nvim', {'branch': 'release', 'commit': '87e5dd692ec8ed7be25b15449fd0ab15a48bfb30', 'do': 'yarn install --frozen-lockfile'}
Plug 'cakebaker/scss-syntax.vim'
Plug 'fatih/vim-go', { 'do': ':GoUpdateBinaries' }

" Productivity

" Plug 'eslint/eslint'
" Plug 'stsewd/fzf-checkout.vim'
Plug 'mattkubej/jest.nvim'
Plug 'preservim/nerdtree'
Plug 'junegunn/fzf', { 'do': { -> fzf#install() } }
Plug 'junegunn/fzf.vim'
Plug 'tpope/vim-fugitive'
Plug 'vim-airline/vim-airline'
Plug 'sbdchd/neoformat'
Plug 'phaazon/hop.nvim'
Plug 'github/copilot.vim'
Plug 'idanarye/vim-merginal'

" UI

Plug 'petertriho/nvim-scrollbar'

" Themes and visuals 

Plug 'dracula/vim', { 'as': 'dracula' }
Plug 'vim-airline/vim-airline-themes'
Plug 'rakr/vim-one'
Plug 'mhartington/oceanic-next'
Plug 'larsbs/vimterial_dark'
Plug 'tomasiser/vim-code-dark'
Plug 'folke/tokyonight.nvim', { 'branch': 'main' }
Plug 'nvim-treesitter/nvim-treesitter', {'do': ':TSUpdate'}

call plug#end()

let g:coc_global_extensions = []

if isdirectory('./node_modules')
  let g:coc_global_extensions += ['coc-css']
  let g:coc_global_extensions += ['coc-tsserver']

  let g:neoformat_try_node_exe = 1
  let g:neoformat_enabled_javascript = ['prettier', 'eslint_d']
  let g:neoformat_run_all_formatters = 1
  let g:neoformat_enabled_python = ['yapf']
  autocmd BufWritePre *.js,*.ts,*.jsx,*.tsx Neoformat
endif

if isdirectory('./node_modules') && isdirectory('./node_modules/eslint')
  let g:coc_global_extensions += ['coc-eslint']
endif

" colorscheme codedark
" colorscheme dracula

let g:tokyonight_style = "night"
colorscheme tokyonight

" colorscheme one
" set background=dark

let g:airline#extensions#tabline#enabled = 1
let g:airline_theme='oceanicnext'

syntax on
let g:oceanic_next_terminal_bold = 1
let g:oceanic_next_terminal_italic = 1

set nu

" set spell
" setlocal spell spelllang=en TODO: check camelCase
set spellfile=~/.vim/spell/en.utf-8.add

set guifont="DejaVu Sans Mono:h13"

" Fix for Enter not working w Coc.nvim
inoremap <silent><expr> <CR> coc#pum#visible() ? coc#pum#confirm() : "\<CR>"

let g:enable_spelunker_vim = 1

" Semi transparent window
highlight Normal guibg=none
highlight NonText guibg=none

" Hotkeys config
nnoremap ff :HopAnywhere<Enter>
nnoremap fw :HopWord<Enter>
nnoremap fg :HopLine<Enter>
nnoremap gd :call CocActionAsync('jumpDefinition')<Enter>

" Init Lua file
lua require('init')

